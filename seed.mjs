import axios from "axios";

let insertUserData = async () => {
    try {
        const { data } = await axios.get("https://jsonplaceholder.typicode.com/users");
        data.map(async user => {
            try {
                await axios.post('http://127.0.0.1:1337/api/auth/local/register', {
                    username: user.name,
                    email: user.email,
                    password: 'Sazzad14',
                });
            } catch (error) {
                console.log(error?.response?.data?.error?.details?.errors ?? error.message)
                console.log(error)
            }

        })
    } catch (error) {
        console.log(error?.response?.data?.error?.details?.errors ?? error.message)
    }
}

let insertPostData = async () => {
    try {
        const { data } = await axios.get("https://jsonplaceholder.typicode.com/posts");
        data.map(async post => {
            try {
                await axios.post('http://127.0.0.1:1337/api/posts', {
                    data: {
                        title: post.title,
                        body: post.body,
                        user: +post.userId
                    }
                });

            } catch (error) {
                console.log(error?.response?.data?.error?.details?.errors ?? error.message)
                console.log(error)
            }
        })
    } catch (error) {
        console.log(error?.response?.data?.error?.details?.errors ?? error.message)
        console.log(error)
    }
}

// insertUserData();
insertPostData();